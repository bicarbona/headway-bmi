<?php

class bmi_calculatorBlock extends HeadwayBlockAPI {

    public $id = 'bmi_calculator';
    public $name = 'Headway BMI calculator';
    public $options_class = 'bmi_calculatorBlockOptions';
    public $description = 'BMI calculator';

    
	function enqueue_action($block_id) {

		/* CSS */
		wp_enqueue_style('headway-simple-slider', plugin_dir_url(__FILE__) . '/css/simple-slider.css');		

		/* JS */
		wp_enqueue_script('headway-simple-slider', plugin_dir_url(__FILE__) . '/js/simple-slider.js', array('jquery'));		

	}
	
	// public static function init_action($block_id, $block) 
    // {

    // }


    // public static function enqueue_action($block_id, $block, $original_block = null)
    // {

    // }


    // public static function dynamic_css($block_id, $block, $original_block = null)
    // {

    // }


	function dynamic_js($block_id, $block = false) {
			if ( !$block )
			$block = HeadwayBlocksData::get_block($block_id);
	
		$js = "
			jQuery(document).ready(function() {
			
			jQuery('#la').bind(
				'slider:changed', function (event, data) {				
					jQuery('#la_value').html(data.value.toFixed(0)); 
					calculateBMI();
				}
			);

			jQuery('#nm').bind(
				'slider:changed', function (event, data) {				
					jQuery('#nm_value').html(data.value.toFixed(0)); 
					calculateBMI();
				}
			);
			
			jQuery('#roi').bind(
				'slider:changed', function (event, data) {				
					jQuery('#roi_value').html(data.value.toFixed(2)); 
					calculateBMI();
				}
			);
			
			jQuery('#cp').bind(
				'slider:changed', function (event, data) {				
					jQuery('#cp_value').html(data.value.toFixed(0)); 
					calculateBMI();
				}
			);
			function calculateBMI(){
				var weight = jQuery('#la_value').html();
				var height = parseInt(jQuery('#nm_value').html())/100;
				var bmi = weight / (height * height);
				
				var type = 'Normal';
				var pos = '';
				if (bmi < 15){
					type= 'Veľmi ťažká ​​podvýživa';
					pos='row_1';
				}else if(bmi <=16){
					type= 'Veľká podváha';
					pos='row_2';
				}else if(bmi <=18.4){
					type= 'Podváha';
					pos='row_3';
				}else if(bmi <=24.9){
					type= 'Normal';
					pos='row_4';
				}else if(bmi <=29.9){
					type= 'Nadváha';
					pos='row_5';
				}else if(bmi <=34.9){
					type= 'Obezita stupeň I (mierna obezita)';
					pos='row_6';
				}else if(bmi <=39.9){
					type= 'Obezita stupeň II (obezita)';
					pos='row_7';
				}else{
					type= 'Obezita stupeň III (vážna obezita)';
					pos='row_8';
				}
				jQuery('tr').removeClass('label-success');
				jQuery('#tbl_bmi').html('<p>'+ bmi.toFixed(1) + '</p><small>'+type+'</small>');
				jQuery('#'+pos).addClass('label-success');
			}
			calculateBMI();

		});
		";
		return $js;
	}

    // public function setup_elements() {
    //     $this->register_block_element(array(
    //         'id' => 'element1-id',
    //         'name' => 'Element 1 Name',
    //         'selector' => '.my-selector1',
    //         'properties' => array('property1', 'property2', 'property3'),
    //         'states' => array(
    //             'Selected' => '.my-selector1.selected',
    //             'Hover' => '.my-selector1:hover',
    //             'Clicked' => '.my-selector1:active'
    //             )
    //         ));
    // }

    public function content($block) {
        /* CODE HERE */
		$html_tag = parent::get_setting($block, 'title-html-tag', 'h1');
		$title_text = parent::get_setting($block, 'title-text', false);

		echo  '<' . $html_tag . ' class="bmi-title">
			'. $title_text .'
		</' . $html_tag . '>';

		echo $bmi_header;
		?>
		
<div class="bmi-calculator">
	<p><span class="label label-primary">Váha <strong><span class='' id="la_value">56</span></strong> kg</span></p>
	<input type="text" data-slider="true" value="56" data-slider-range="15,240" data-slider-step=".5" data-slider-snap="true" id="la">
	<p><span class="label label-danger">Výška <strong><span class =''  id="nm_value">160</span> </strong> cm</span></p>
	<input type="text" data-slider="true" value="160" data-slider-range="130,210" data-slider-step="1" data-slider-snap="true" id="nm">
</div>
	<div class="alert">									  
	<strong>BMI hodnota</strong> </br>
	  <div id='tbl_bmi'></div>
	</div>

	<table class='table'>
		<thead>
			<tr>
				<th>BMI Index</th>
				<th>Význam</th>
			</tr>
		</thead>
		
		<tbody>
			<tr id='row_1'>
				<td>Pod 15</td>
				<td>veľmi ťažká ​​podvýživa</td>
			</tr>
			<tr id='row_2'>
				<td>15,0 - 16,0</td>
				<td>veľká podváha</td>
			</tr>
			<tr id='row_3'>
				<td>16,0 - 18,4</td>
				<td>podváha</td>
			</tr>
			<tr id='row_4'>
				<td>18,5 - 24.9</td>
				<td>normal</td>
			</tr>
			<tr id='row_5'>
				<td>25 - 29,9</td>
				<td>nadváha</td>
			</tr>
			<tr id='row_6'>
				<td>30 - 34,9</td>
				<td>obezita stupeň I (mierna obezita)</td>
			</tr>
			<tr id='row_7'>
				<td>35 - 39,9</td>
				<td>obezita stupeň II (obezita)</td>
			</tr>
			<tr id='row_8'>
				<td>Nad 40</td>
				<td>obezita stupeň III (vážna obezita)</td>
			</tr>
			
		</tbody>
	</table>
		<?php
    }
}