<?php
/**
	Plugin Name: Headway BMI calculator
	Plugin URI:  https://bitbucket.org/bicarbona/headway-bmi
	Description: BMI calculator
	Version:     0.2.6
	Author:      bicarbona
	Author URI:  
	License:     GPLv2+
	Text Domain: bmi_calculator
	Domain Path: /languages
 	Bitbucket Plugin URI: https://bitbucket.org/bicarbona/headway-bmi
 	Bitbucket Branch: master
*/

//* Bitbucket Plugin URI: https://bicarbona:lucuska@bitbucket.org/bicarbona/headway-bmi

define('BMI_CALCULATOR_BLOCK_VERSION', '0.1.0');

/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'bmi_calculator_register');
function bmi_calculator_register() {

	if ( !class_exists('Headway') )
		return;

	require_once 'includes/Block.php';
	require_once 'includes/BlockOptions.php';
	//require_once 'design-editor-settings.php';

	return headway_register_block('bmi_calculatorBlock', plugins_url(false, __FILE__));
}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'bmi_calculator_prevent_404');
function bmi_calculator_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/**
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'bmi_calculator_redirect');
function bmi_calculator_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;
}